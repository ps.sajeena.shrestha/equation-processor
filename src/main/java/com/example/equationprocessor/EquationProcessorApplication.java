package com.example.equationprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EquationProcessorApplication {

    public static void main(String[] args) {
        SpringApplication.run(EquationProcessorApplication.class, args);
    }

}
