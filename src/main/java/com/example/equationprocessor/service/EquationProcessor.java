package com.example.equationprocessor.service;

import com.example.equationprocessor.constant.Operators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

import static com.example.equationprocessor.constant.Operators.*;

public class EquationProcessor {

    private final static Logger logger = LogManager.getLogger(EquationProcessor.class);

    private final Stack<Double> operands;
    private final Stack<String> operations;
    private final Map<String, Double> variableMap;
    private Scanner scanner;

    public EquationProcessor() {
        variableMap = new HashMap<>();
        this.operands = new Stack<>();
        this.operations = new Stack<>();
    }

    public Double solve() {
        String equation = getEquation();
        String[] equationArray = equation.split("(?<=\\w?)(?=\\W)|(?<=\\W)(?=\\w?)");
        System.out.println(Arrays.toString(equationArray));

        for (String value : equationArray) {
            pushValue(value);
            printStack();
        }

        while (!operands.empty() && !operations.empty()) {
            performOperation();
            printStack();
        }

        return operands.peek();
    }

    private String getEquation() {
        scanner = new Scanner(System.in);
        String equation;
        System.out.print("Enter equation : ");
        equation = scanner.nextLine();
        return equation;
    }

    private void pushValue(String value) {
        if (isNumeric(value)) {
            operands.push(Double.parseDouble(value));
        } else if (isVariable(value)) {
            pushVariable(value);
        } else if (isCloseBracket(value.trim())) {
            while (!operations.peek().equals("(")) {
                performOperation();
                printStack();
            }
            operations.pop();
        } else if (isOperator(value)) {
            checkOperatorPriority(value);
            operations.push(value);
        } else {
            throw new IllegalStateException("Invalid Equation");
        }
    }

    private boolean isNumeric(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean isVariable(String value) {
        String alphabetRegex = "[a-zA-Z]*";
        return value.matches(alphabetRegex);
    }

    private boolean isCloseBracket(String value) {
        return value.equals(")");
    }

    private boolean isBrackets(String value) {
        String[] brackets = {"(", ")"};
        return Arrays.asList(brackets).contains(value);
    }

    private boolean isOperator(String value) {
        String operatorRegex = "[()+*/^-]";
        return value.matches(operatorRegex);
    }

    private void pushVariable(String value) {
        Double enteredValue;
        if (variableMap.containsKey(value)) {
            enteredValue = variableMap.get(value);
        } else {
            enteredValue = getVariable(value);
        }
        operands.push(enteredValue);
    }

    private void checkOperatorPriority(String value) {
        if (!operations.empty() && !operands.empty()) {
            if (hasLowestPriority(value.trim(), operations.peek().trim())) {
                performOperation();
                printStack();
                checkOperatorPriority(value);
            }
        }
    }

    private Double getVariable(String value) {
        Double enteredValue;
        System.out.print("Enter value of " + value + ":");
        try {
            enteredValue = Double.parseDouble(scanner.nextLine());
        } catch (NumberFormatException ex) {
            throw new InputMismatchException("Entered value is not valid");
        }
        variableMap.put(value, enteredValue);
        return enteredValue;
    }

    private boolean hasLowestPriority(String value, String peekOperation) {
        if (isBrackets(value) || isBrackets(peekOperation)) {
            return false;
        } else {
            return getOperator(value).compareTo(getOperator(peekOperation)) <= 0;
        }
    }

    private Operators getOperator(String value) {
        switch (value) {
            case "-":
                return SUBTRACTION;
            case "+":
                return ADDITION;
            case "/":
                return DIVISION;
            case "*":
                return MULTIPLICATION;
            case "^":
                return POWER;
            default:
                throw new IllegalStateException("Invalid Operator");
        }
    }

    private void performOperation() {
        try {
            String peekOperation = operations.peek().trim();
            Double firstElement = operands.pop();
            Double secondElement = operands.pop();
            double result = getOperator(peekOperation).solve(firstElement, secondElement);
            operands.push(result);
            operations.pop();
        } catch (EmptyStackException ex) {
            throw new IllegalStateException("Invalid Equation");
        }
    }

    private void printStack() {
        System.out.println("************************");
        logger.info("operands : {}", operands);
        logger.info("operations : {}", operations);
    }

}
