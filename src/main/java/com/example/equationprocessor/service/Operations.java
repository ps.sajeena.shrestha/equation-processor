package com.example.equationprocessor.service;

public interface Operations {

    Double solve(Double firstElement, Double secondElement);

}
