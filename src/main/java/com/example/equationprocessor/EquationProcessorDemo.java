package com.example.equationprocessor;

import com.example.equationprocessor.service.EquationProcessor;

public class EquationProcessorDemo {

    public static void main(String[] args) {
        EquationProcessor equationProcessor = new EquationProcessor();
        equationProcessor.solve();
    }
}