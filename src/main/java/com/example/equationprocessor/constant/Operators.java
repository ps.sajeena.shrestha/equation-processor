package com.example.equationprocessor.constant;

import com.example.equationprocessor.service.Operations;

import static java.lang.Math.pow;

public enum Operators implements Operations {

    SUBTRACTION {
        @Override
        public Double solve(Double firstElement, Double secondElement) {
            return secondElement - firstElement;
        }
    },
    ADDITION {
        @Override
        public Double solve(Double firstElement, Double secondElement) {
            return secondElement + firstElement;
        }
    },
    DIVISION {
        @Override
        public Double solve(Double firstElement, Double secondElement) {
            return secondElement / firstElement;
        }
    },
    MULTIPLICATION {
        @Override
        public Double solve(Double firstElement, Double secondElement) {
            return secondElement * firstElement;
        }
    },
    POWER {
        @Override
        public Double solve(Double firstElement, Double secondElement) {
            return pow(secondElement, firstElement);
        }
    };

}
