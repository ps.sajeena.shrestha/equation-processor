package com.example.equationprocessor.service;

import com.example.equationprocessor.EquationProcessorApplicationTests;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EquationProcessorTest extends EquationProcessorApplicationTests {

    private EquationProcessor equationProcessor;
    private static final String END_OF_LINE = System.getProperty("line.separator");

    @BeforeEach
    public void setUp() {
        equationProcessor = new EquationProcessor();
    }

    @Test
    @DisplayName("Given equation, when solve returns answer")
    public void givenEquation_whenSolve_thenReturnAnswer() {
        String equation = "31+5*6-7";
        System.setIn(new ByteArrayInputStream(equation.getBytes()));

        Double result = equationProcessor.solve();

        assertEquals(54, result, "Answer of the equation should be fifty four");
    }

    @Test
    @DisplayName("Given incorrect equation, when solve throws exception")
    public void givenIncorrectEquation_whenSolve_thenThrowException() {
        String equation = "31+5*+6-7";
        System.setIn(new ByteArrayInputStream(equation.getBytes()));

        assertThrows(IllegalStateException.class, () -> equationProcessor.solve(),
                "It should throw illegal input exception ");
    }

    @Test
    @DisplayName("Given invalid equation, when solve throws exception")
    public void givenInvalidEquation_whenSolve_thenThrowException() {
        String equation = "31+#@5*6-7";
        System.setIn(new ByteArrayInputStream(equation.getBytes()));

        assertThrows(IllegalStateException.class, () -> equationProcessor.solve(),
                "It should throw illegal input exception ");
    }

    @Test
    @DisplayName("Given equation with input , when solve returns answer")
    public void givenEquationWithInput_whenSolve_thenReturnAnswer() {
        String equation = "3+5*x-y";
        String x = "4";
        String y = "2";
        String simulatedInput = equation + END_OF_LINE + x + END_OF_LINE + y + END_OF_LINE;
        System.setIn(new ByteArrayInputStream(simulatedInput.getBytes(StandardCharsets.UTF_8)));

        Double result = equationProcessor.solve();

        assertEquals(21, result, "Answer of the equation should be twenty one");
    }

    @Test
    @DisplayName("Given incorrect equation with input, when solve throws exception")
    public void givenIncorrectEquationWithInput_whenSolve_thenThrowException() {
        String equation = "3+5*x+-y";
        String x = "4";
        String y = "2";
        String simulatedInput = equation + END_OF_LINE + x + END_OF_LINE + y + END_OF_LINE;
        System.setIn(new ByteArrayInputStream(simulatedInput.getBytes(StandardCharsets.UTF_8)));

        assertThrows(IllegalStateException.class, () -> equationProcessor.solve(),
                "It should throw illegal input exception ");
    }

    @Test
    @DisplayName("Given equation with and without bracket, when solve return different answer ")
    public void givenEquationWithAndWithoutBracket_whenSolve_thenReturnDifferentAnswer() {
        String equation = "3+(8-2)*x-y";
        String secondEquation = "3+8-2*x-y";
        String x = "4";
        String y = "2";

        String simulatedInput = equation + END_OF_LINE + x + END_OF_LINE + y + END_OF_LINE;
        System.setIn(new ByteArrayInputStream(simulatedInput.getBytes(StandardCharsets.UTF_8)));
        Double result = equationProcessor.solve();

        equationProcessor = new EquationProcessor();
        String secondSimulatedInput = secondEquation + END_OF_LINE + x + END_OF_LINE + y + END_OF_LINE;
        System.setIn(new ByteArrayInputStream(secondSimulatedInput.getBytes(StandardCharsets.UTF_8)));
        Double secondResult = equationProcessor.solve();

        assertEquals(25, result, "Answer of the equation should be twenty five");
        assertEquals(1, secondResult, "Answer of the equation should be one");
    }

    @Test
    @DisplayName("Given equation with power, when solve return answer ")
    public void givenEquationWithPower_whenSolve_thenReturnAnswer() {
        String equation = "3*x^y-10";
        String x = "4";
        String y = "2";
        String simulatedInput = equation + END_OF_LINE + x + END_OF_LINE + y + END_OF_LINE;
        System.setIn(new ByteArrayInputStream(simulatedInput.getBytes(StandardCharsets.UTF_8)));

        Double result = equationProcessor.solve();

        assertEquals(38, result, "Answer of the equation should be thirty eight");
    }

}